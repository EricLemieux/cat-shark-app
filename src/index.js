import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunkMiddleware from 'redux-thunk'
import { receiveCategoryData } from './Actions';
import reducer from './Reducers';
import App from './Containers/App';
import './index.css';

const default_state = {
  loading: true,
  images: [],
  current_image_id: 0,
  prev_image_id: 0,
  next_image_id: 0
};

const store = createStore(
  reducer, 
  default_state, 
  applyMiddleware(
    thunkMiddleware
));

ReactDOM.render(
  <Provider store={store}>
    <App dispatch={store.dispatch} />
  </Provider>, document.getElementById('app-root'));
