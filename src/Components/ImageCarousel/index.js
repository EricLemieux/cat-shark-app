import React from 'react';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import { faArrowLeft, faArrowRight } from '@fortawesome/fontawesome-free-solid';
import './ImageCarousel.css';

const ImageCarousel = ({prevImage, currentImage, nextImage, prevImageClick, nextImageClick}) => (
  <div className="image-carousel">
    <div className="image-carousel--button-column">
      {prevImage !== currentImage && 
        <button onClick={() => prevImageClick()} className="image-carousel--button">
          <FontAwesomeIcon icon={faArrowLeft} title="Previous image" size="3x" />
        </button>
      }
    </div>
    
    <div className="image-carousel--image-column">
      <img src={currentImage} className="image-carousel--image" />
    </div>
    
    <div className="image-carousel--button-column">
      {nextImage !== currentImage &&
        <button onClick={() => nextImageClick()} className="image-carousel--button">
          <FontAwesomeIcon icon={faArrowRight} title="Next image" size="3x" />
        </button>
      }
    </div>
  </div>
);
export default ImageCarousel;
