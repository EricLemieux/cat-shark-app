import React from 'react';
import './CategoryButtons.css';

const CategoryButtons = ({availableCategories, selectedCategories, buttonClick}) => (
  <div className="category-button-list">
    <h2 className="category-button-list--label">Select a category:</h2>
    {availableCategories.map(cat =>
      <button 
        className={`category-button ${selectedCategories.indexOf(cat) !== -1 ? 'category-button_selected' : []}`} 
        key={cat}
        onClick={() => {buttonClick(cat)}}
      >
        {cat}
      </button>
    )}
  </div>
);
export default CategoryButtons;
