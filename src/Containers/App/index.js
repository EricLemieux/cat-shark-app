import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setImages, receiveCategoryData } from '../../Actions';
import CarouselContainer from '../CarouselContainer';
import ImageCategories from '../ImageCategories';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/fontawesome-free-solid';
import './App.css'

class App extends Component {
  componentWillMount() {
    this.props.dispatch(receiveCategoryData());
  }

  render() {
    return (
      <div>
        <h1>Cat shark app</h1>
        <ImageCategories />
        <CarouselContainer />
        {this.props.isLoading && 
          <div className="loading-spinner">
            <FontAwesomeIcon icon={faSpinner} title="Loading" size="3x" color="white" spin />
          </div>
        }
      </div>
    );
  }
};

const mapStateToProps = (state) => ({
  isLoading: state.loading
});

App = connect(mapStateToProps)(App)
export default App;
