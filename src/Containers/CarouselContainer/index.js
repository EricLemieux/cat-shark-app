import React from 'react';
import { connect } from 'react-redux';
import ImageCarousel from '../../Components/ImageCarousel';
import { prevImage, nextImage } from '../../Actions';

const mapStateToProps = (state) => ({
  prevImage: (!isNaN(state.prev_image_id) && state.images) ? state.images[state.prev_image_id] : null,
  currentImage: (!isNaN(state.current_image_id) && state.images) ? state.images[state.current_image_id] : null,
  nextImage: (!isNaN(state.next_image_id) && state.images) ? state.images[state.next_image_id] : null
});

const mapDispatchToProps = {
  prevImageClick: prevImage,
  nextImageClick: nextImage,
};

const CarouselContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ImageCarousel);
export default CarouselContainer;
