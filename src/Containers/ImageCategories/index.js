import React from 'react';
import { connect } from 'react-redux';
import CategoryButtons from '../../Components/CategoryButtons';
import { clickCategoryButton } from '../../Actions';

const mapStateToProps = (state) => ({
  availableCategories: state.available_categories || [],
  selectedCategories: state.selected_categories || []
});

const mapDispatchToProps = {
  buttonClick: clickCategoryButton
};

const ImageCategories = connect(
  mapStateToProps,
  mapDispatchToProps
)(CategoryButtons);
export default ImageCategories;