export const TOGGLE_LOADING = 'TOGGLE_LOADING';
export const SET_IMAGES = 'SET_IMAGES';
export const NEXT_IMAGE = 'NEXT_IMAGE';
export const PREV_IMAGE = 'PREV_IMAGE';
export const SET_AVAILABLE_CATEGORIES = 'SET_AVAILABLE_CATEGORIES';
export const TOGGLE_CATEGORY = 'TOGGLE_CATEGORY';
export const FETCH_IMAGES = 'FETCH_IMAGES';

export function toggleLoading() {
  return {type: TOGGLE_LOADING};
}

export function setImages(images) {
  return {type: SET_IMAGES, images}
}

export function nextImage() {
  return {type: NEXT_IMAGE};
}

export function prevImage() {
  return {type: PREV_IMAGE};
}

export function setAvailableCategories(categories) {
  return {type: SET_AVAILABLE_CATEGORIES, categories};
}

export function receiveCategoryData() {
  return (dispatch, getState) => {
    dispatch(() => {
      return fetch('/api/v1/image_categories')
        .then(response => response.json())
        .then(json => {
          dispatch(setAvailableCategories(json.categories || []));
          dispatch(fetchImages());
        });
    });
  };
}

export function toggleCategory(category) {
  return {type: TOGGLE_CATEGORY, category}
}

export function fetchImages() {
  return (dispatch, getState) => {
    dispatch(toggleLoading());
    const state = getState();
    dispatch(() => {
      let category_query = '';
      state.selected_categories && state.selected_categories.map((cat) => category_query = `${category_query}&categories[]=${cat}`);
  
      return fetch(`/api/v1/images?${category_query}`)
        .then(response => response.json())
        .then(json => dispatch(setImages(json.images || [])));
    });
  }
}

export function clickCategoryButton(category) {
  return (dispatch, getState) => {
    dispatch(toggleCategory(category));
    dispatch(fetchImages());
  };
}
