import * as actions from './index';

describe('Carousel actions', () => {
  it('Toggle loading', () => {
    expect(actions.toggleLoading()).toEqual({
      type: 'TOGGLE_LOADING'
    });
  });

  it('Set images', () => {
    expect(actions.setImages(["img1.jpg", "img2.jpg"])).toEqual({
      type: 'SET_IMAGES',
      images: [
        "img1.jpg",
        "img2.jpg"
      ]
    });
  });

  it('Next image', () => {
    expect(actions.nextImage()).toEqual({
      type: 'NEXT_IMAGE'
    });
  });

  it('Previous image', () => {
    expect(actions.prevImage()).toEqual({
      type: 'PREV_IMAGE'
    });
  });

  it('Sets available categories', () => {
    expect(actions.setAvailableCategories(['foo', 'bar'])).toEqual({
      type: 'SET_AVAILABLE_CATEGORIES',
      categories: [
        'foo',
        'bar'
      ]
    });
  });

  it('Toggle category', () => {
    expect(actions.toggleCategory('test')).toEqual({
      type: 'TOGGLE_CATEGORY',
      category: 'test'
    });
  });
});
