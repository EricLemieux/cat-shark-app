import {
  TOGGLE_LOADING,
  SET_IMAGES,
  NEXT_IMAGE,
  PREV_IMAGE,
  SET_AVAILABLE_CATEGORIES,
  TOGGLE_CATEGORY,
  RECEIVE_IMAGES
} from '../Actions';

const clamp = (num, min, max) => Math.max(Math.min(num, max), min);

const determine_prev_image = (current_image, images) => !isNaN(current_image) && images && images.length > 0 ? clamp(current_image - 1, 0, images.length - 1) : null;
const determine_next_image = (current_image, images) => !isNaN(current_image) && images && images.length > 0 ? clamp(current_image + 1, 0, images.length - 1) : null;

const reducer = (state = [], action) => {
  switch(action.type) {
    case TOGGLE_LOADING: {
      return Object.assign({}, state, {loading: !state.loading});
    }
    case SET_IMAGES: {
      let current_image_id = action.images && action.images.length > 0 ? 0 : null;
      let prev_image_id = determine_prev_image(current_image_id, action.images);
      let next_image_id = determine_next_image(current_image_id, action.images);

      return Object.assign({}, state, {
        images: action.images || [], 
        loading: false,
        current_image_id,
        prev_image_id,
        next_image_id
      });
    }
    case NEXT_IMAGE: {
      // Quickly exit if there are no images
      if(!state.images || state.images.length < 1) {
        return Object.assign({}, state, {
          current_image_id: null,
          prev_image_id: null,
          next_image_id: null
        });
      }

      let current_image_id = !isNaN(state.next_image_id) ? state.next_image_id : clamp(state.current_image_id + 1, 0, state.images.length-1);
      let prev_image_id = determine_prev_image(current_image_id, state.images);
      let next_image_id = determine_next_image(current_image_id, state.images);

      return Object.assign({}, state, {
        current_image_id,
        prev_image_id,
        next_image_id
      });
    }
    case PREV_IMAGE: {
      // Quickly exit if there are no images
      if(!state.images || state.images.length < 1) {
        return Object.assign({}, state, {
          current_image_id: null,
          prev_image_id: null,
          next_image_id: null
        });
      }

      let current_image_id = !isNaN(state.prev_image_id) ? state.prev_image_id : clamp(state.current_image_id - 1, 0, state.images.length-1);
      let prev_image_id = determine_prev_image(current_image_id, state.images);
      let next_image_id = determine_next_image(current_image_id, state.images);

      return Object.assign({}, state, {
        current_image_id,
        prev_image_id,
        next_image_id
      });
    }
    case SET_AVAILABLE_CATEGORIES: {
      return Object.assign({}, state, {
        available_categories: action.categories || [],
        selected_categories: action.categories && action.categories.length > 0 ? [action.categories[0]] : []
      });
    }
    case TOGGLE_CATEGORY: {
      let selected_categories = state.selected_categories || [];
      let available_categories = state.available_categories || [];

      if (action.category && available_categories.indexOf(action.category) !== -1) {
        if (selected_categories.indexOf(action.category) !== -1) {
          selected_categories = selected_categories.filter(e => e !== action.category);
        } else {
          selected_categories = [...new Set([...selected_categories, action.category])];
        }
      }
      
      return Object.assign({}, state, {
        selected_categories, 
        available_categories
      });
    }
    default: {
      return state;
    }
  }
};
export default reducer;
