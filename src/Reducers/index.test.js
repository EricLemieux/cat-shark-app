import reducer from './index';
import { 
  TOGGLE_LOADING,
  SET_IMAGES,
  NEXT_IMAGE,
  PREV_IMAGE,
  SET_AVAILABLE_CATEGORIES,
  TOGGLE_CATEGORY } from '../Actions';

describe('Carousel reducer', () => {
  it('Initial state', () => {
    expect(reducer({}, {})).toEqual({});
  });

  it('Toggles loading', () => {
    // Loading not set, then toggled.
    expect(reducer({}, {
      type: TOGGLE_LOADING
    })).toEqual({
      loading: true
    });

    // Loading set to true, then toggled.
    expect(reducer({
      loading: true
    }, {
      type: TOGGLE_LOADING
    })).toEqual({
      loading: false
    });

    // Loading set to false, then toggled.
    expect(reducer({
      loading: false
    }, {
      type: TOGGLE_LOADING
    })).toEqual({
      loading: true
    });
  });

  it('Sets images', () => {
    expect(reducer({}, {
      type: SET_IMAGES,
      images: []
    })).toEqual({
      images: [],
      loading: false,
      current_image_id: null,
      next_image_id: null,
      prev_image_id: null
    });

    // Setting images when none exist currently.
    expect(reducer({}, {
      type: SET_IMAGES,
      images: [
        'img1.jpg',
        'img2.jpg'
      ]
    })).toEqual({
      images: [
        'img1.jpg',
        'img2.jpg'
      ],
      loading: false,
      current_image_id: 0,
      next_image_id: 1,
      prev_image_id: 0
    });

    // Setting images to nothing when some already exist.
    expect(reducer({
      images: [
        'img1.jpg',
        'img2.jpg'
      ]
    }, {
      type: SET_IMAGES,
      images: []
    })).toEqual({
      images: [],
      loading: false,
      current_image_id: null,
      next_image_id: null,
      prev_image_id: null
    });

    // Fails gracefully when images are not provided.
    expect(reducer({
      images: [
        'img1.jpg',
        'img2.jpg'
      ]
    }, {
      type: SET_IMAGES
    })).toEqual({
      images: [],
      loading: false,
      current_image_id: null,
      next_image_id: null,
      prev_image_id: null
    });
  });

  it('Advances to the next image', () => {
    // When no current image is set return null so nothing will be displayed.
    expect(reducer({}, {
      type: NEXT_IMAGE
    })).toEqual({
      current_image_id: null,
      next_image_id: null,
      prev_image_id: null
    });

    // Try to advance to the next image when no images are set.
    expect(reducer({
      current_image_id: 0
    }, {
      type: NEXT_IMAGE
    })).toEqual({
      current_image_id: null,
      next_image_id: null,
      prev_image_id: null
    });

    // Set the images and then advance to the next one.
    expect(reducer({
      images: [
        'img1.jpg',
        'img2.jpg'
      ],
      current_image_id: 0
    }, {
      type: NEXT_IMAGE
    })).toEqual({
      images: [
        'img1.jpg',
        'img2.jpg'
      ],
      current_image_id: 1,
      next_image_id: 1,
      prev_image_id: 0
    });

    // Try to advance the current image when there is more than enough images in the array
    expect(reducer({
      images: [
        'img1.jpg',
        'img2.jpg',
        'img3.jpg'
      ],
      current_image_id: 0
    }, {
      type: NEXT_IMAGE
    })).toEqual({
      images: [
        'img1.jpg',
        'img2.jpg',
        'img3.jpg'
      ],
      current_image_id: 1,
      next_image_id: 2,
      prev_image_id: 0
    });

    // Try to advance to the next image when at the last image in the array.
    expect(reducer({
      images: [
        'img1.jpg',
        'img2.jpg'
      ],
      current_image_id: 1
    }, {
      type: NEXT_IMAGE
    })).toEqual({
      images: [
        'img1.jpg',
        'img2.jpg'
      ],
      current_image_id: 1,
      next_image_id: 1,
      prev_image_id: 0
    });
  });

  it('Reverts to the previous image', () => {
    // When no current image is set return null so nothing will be displayed.
    expect(reducer({}, {
      type: PREV_IMAGE
    })).toEqual({
      current_image_id: null,
      next_image_id: null,
      prev_image_id: null
    });

    // Try to reverse to the previous image when no images are set.
    expect(reducer({
      current_image_id: 0
    }, {
      type: PREV_IMAGE
    })).toEqual({
      current_image_id: null,
      next_image_id: null,
      prev_image_id: null
    });

    // Try to reverse the current image when there is more than enough images in the array
    expect(reducer({
      images: [
        'img1.jpg',
        'img2.jpg',
        'img3.jpg'
      ],
      current_image_id: 2
    }, {
      type: PREV_IMAGE
    })).toEqual({
      images: [
        'img1.jpg',
        'img2.jpg',
        'img3.jpg'
      ],
      current_image_id: 1,
      next_image_id: 2,
      prev_image_id: 0
    });

    // Try to reverse the current image when at the start of the array
    expect(reducer({
      images: [
        'img1.jpg',
        'img2.jpg'
      ],
      current_image_id: 0
    }, {
      type: PREV_IMAGE
    })).toEqual({
      images: [
        'img1.jpg',
        'img2.jpg'
      ],
      current_image_id: 0,
      next_image_id: 1,
      prev_image_id: 0
    });

    // Reverse the current image when at the end of the array.
    expect(reducer({
      images: [
        'img1.jpg',
        'img2.jpg'
      ],
      current_image_id: 1
    }, {
      type: PREV_IMAGE
    })).toEqual({
      images: [
        'img1.jpg',
        'img2.jpg'
      ],
      current_image_id: 0,
      next_image_id: 1,
      prev_image_id: 0
    });
  });

  it('Sets the list of available categories', () => {
    // Base case where categories are set properly.
    expect(reducer({}, {
      type: SET_AVAILABLE_CATEGORIES,
      categories: [
        'foo',
        'bar'
      ]
    })).toEqual({
      available_categories: [
        'foo',
        'bar'
      ],
      selected_categories: [
        'foo'
      ]
    });

    // Removing currently set categories.
    expect(reducer({
      available_categories: [
        'foo',
        'bar'
      ]
    }, {
      type: SET_AVAILABLE_CATEGORIES,
      categories: []
    })).toEqual({
      available_categories: [],
      selected_categories: []
    });

    // Setting categories with an empty array.
    expect(reducer({}, {
      type: SET_AVAILABLE_CATEGORIES,
      categories: []
    })).toEqual({
      available_categories: [],
      selected_categories: []
    });

    // Not including the categories property.
    expect(reducer({}, {
      type: SET_AVAILABLE_CATEGORIES
    })).toEqual({
      available_categories: [],
      selected_categories: []
    });
  });

  it('Adds a category to the selected list', () => {
    // Add a category from one of the available.
    expect(reducer({
      available_categories: [
        'foo'
      ]
    }, {
      type: TOGGLE_CATEGORY,
      category: 'foo'
    })).toEqual({
      available_categories: [
        'foo'
      ],
      selected_categories: [
        'foo'
      ]
    });

    // Trying to add a category that is not one of the available ones.
    expect(reducer({
      available_categories: [
        'foo'
      ]
    }, {
      type: TOGGLE_CATEGORY,
      category: 'bar'
    })).toEqual({
      available_categories: [
        'foo'
      ],
      selected_categories: []
    });

    // Trying to add a category when there are none available.
    expect(reducer({
      available_categories: []
    }, {
      type: TOGGLE_CATEGORY,
      category: 'bar'
    })).toEqual({
      available_categories: [],
      selected_categories: []
    });

    // Trying to add a category when available categories is null.
    expect(reducer({}, {
      type: TOGGLE_CATEGORY,
      category: 'bar'
    })).toEqual({
      available_categories: [],
      selected_categories: []
    });
  });

  it('Removes a category from the selected list', () => {
    // Removes a category from one of the available.
    expect(reducer({
      available_categories: [
        'foo'
      ],
      selected_categories: [
        'foo'
      ]
    }, {
      type: TOGGLE_CATEGORY,
      category: 'foo'
    })).toEqual({
      available_categories: ['foo'],
      selected_categories: []
    });

    // Trying to remove a category when the selected is not part of the list.
    expect(reducer({
      selected_categories: ['foo']
    }, {
      type: TOGGLE_CATEGORY,
      category: 'bar'
    })).toEqual({
      available_categories: [],
      selected_categories: ['foo']
    });

    // Trying to remove a category when there are none available.
    expect(reducer({
      selected_categories: []
    }, {
      type: TOGGLE_CATEGORY,
      category: 'bar'
    })).toEqual({
      available_categories: [],
      selected_categories: []
    });

    // Trying to remove a category when available categories is null.
    expect(reducer({}, {
      type: TOGGLE_CATEGORY,
      category: 'bar'
    })).toEqual({
      available_categories: [],
      selected_categories: []
    });
  });
});
