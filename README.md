# Cat Shark App

Simple react image carousel app using react, redux, webpack, babel, node, and express.

Image data is pulled from a json file on the server. 
This feeds into two api endpoints, one for the image categories (`cat`, `shark`)
and the other endpoint returns a list of images that fit the requested categories.

## Install
* `yarn`

## Running
* `yarn start`

## Testing
* Test once: `yarn test`
* Test watcher: `yarn test --watch`
* Test coverage: `yarn test --coverage`

## Still to do if I had more time
* More testing
  * Server is missing tests
  * React components could of had snapshot testing
  * More complete redux test coverage
* Defining prop types for all react components
* More complete documentation 
