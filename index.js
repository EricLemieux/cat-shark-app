const express = require('express');
const path = require('path');
const app = express();
const image_categories = require('./categories.json');

// Uses the Fisher–Yates shuffle to randomly shuffle the array.
const shuffleArray = (arr) => {
  for(let i = arr.length - 1; i > 0; --i) {
    const j = Math.floor(Math.random() * (i - 1));
    [arr[i], arr[j]] = [arr[j], arr[i]];
  }

  return arr;
};

app.set('port', process.env.PORT || 8001);

// Serve static assets, such as the base html.
app.use(express.static(path.join(__dirname, 'public')));

// Serve built assets, such as the react app after it has been packaged.
app.use(express.static(path.join(__dirname, 'dist')));

// Api endpoint for the image categories.
app.get('/api/v1/image_categories', (req, res) => {
  let categories = [];

  image_categories.map(cat => categories.push(cat.name));

  res.json({
    categories
  });
});

// Api endpoint for the images.
app.get('/api/v1/images', (req, res) => {
  const query_categories = req.query.categories || [];

  // Determine category of images.
  let images = [];
  query_categories.map((category) => {
    const category_id = image_categories.map(img_cat => img_cat.name).indexOf(category);
    if (category_id >= 0) {
      images.push(...image_categories[category_id].images);
    }
  });

  // Shuffle the image array so they arrive in a random order.
  images = shuffleArray(images);

  // Return that list of images.
  res.json({
    images
  });
});

const server = app.listen(app.get('port'), () => {
  console.log(`Server started on port: ${server.address().port}`);
});
